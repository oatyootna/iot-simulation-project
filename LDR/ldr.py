#!/usr/bin/python
import RPi.GPIO as GPIO
import os
import paho.mqtt.client as mqtt
import sys
import time
import Adafruit_DHT


GPIO.setmode(GPIO.BCM)
GPIO.setup(4,GPIO.IN)

#IP address of MQTT Broker
broker_address = "192.168.1.8"

#client = mqtt.Client()
#client.connect(broker_address, 1883, 61)

while (True):
    client = mqtt.Client()
    client.connect(broker_address, 1883, 61) 
    sensor_data = "light_sensor status="+str(GPIO.input(4)) 
#    print sensor_data
    client.publish("sensors", sensor_data )
    client.disconnect()
    time.sleep(300)


#client.disconnect()
