#!/bin/bash


dc="Thammasat"
user="ubuntu"
host="com"
url="https://flynn.sci.tu.ac.th:8086"
database="room213_computers"
for ((i = 0; i < ${1}; i++))
do
	num=`expr $i + 1`
	dir="./Telegraf_"$user$num
         
	mkdir $dir
        cp docker-compose.yml $dir
        cp telegraf.conf $dir
        
	echo 'USER="'$user$num'"' > $dir"/"default_config
        echo 'DC="'$dc'"' >> $dir"/"default_config
	echo 'INFLUX_URL="'$url'"' >> $dir"/"default_config
        echo 'TAG="'$num'"' >> $dir"/"default_config
	echo 'DATABASE="'$database'"' >> $dir"/"default_config
	echo 'HOSTNAME="'$host$num'"' >> $dir"/"default_config
        
done
