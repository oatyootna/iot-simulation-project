import RPi.GPIO as GPIO
from datetime import datetime
from time import sleep
import paho.mqtt.client as mqtt

broker_address = "xx.xx.xx"

GPIO.setmode(GPIO.BCM)
GPIO.setup(4,GPIO.IN)

loop_minutes = 5
sec_zero = False

while True:
    sleep(1)
    minutes = int(datetime.now().strftime("%M"))
    sec = int(datetime.now().strftime("%S"))
    for s in range(0,3):
        if (sec%60) == s:
            sec_zero = True
    if (minutes%loop_minutes) == 0 and sec_zero :
        #f = open("light_status.log", "a")
        #f.write(str(datetime.now()) + " light_status = " + str(GPIO.input(4)) + "\n")
        #f.close()
        client = mqtt.Client()
        client.connect(broker_address, 1883, 61)
        sensor_data = "light_status ldr_status="+str(GPIO.input(4))
        client.publish("sensors", sensor_data)
        client.disconnect()
        
        sec_zero = False
        sleep((loop_minutes*60)-10)
