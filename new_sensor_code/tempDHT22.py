#!/usr/bin/python
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import sys
import Adafruit_DHT
from time import sleep
from datetime import datetime

sensor = Adafruit_DHT.DHT22
pin = 8

loop_minutes = 5
sec_zero = False

while True:
    sleep(1)
    minutes = int(datetime.now().strftime("%M"))
    sec = int(datetime.now().strftime("%S"))
    for s in range(0,3):
        if (sec%60) == s:
            sec_zero = True
    if (minutes%loop_minutes) == 0 and sec_zero :
        humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, 8)
        if humidity is not None and temperature is not None:
            f = open("DHT22.log", "a")
            f.write(str(datetime.now())+" ")
            f.write('Temp={0:0.1f}C  Humidity={1:0.1f}%\n'.format(temperature, humidity))
            f.close()
        else:
            sys.exit(1)
        sec_zero = False
        sleep((loop_minutes*60)-10)




  

