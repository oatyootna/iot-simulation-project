#!/usr/bin/python
import os
import paho.mqtt.client as mqtt
import sys
import time
import Adafruit_DHT


sensor = Adafruit_DHT.DHT22
pin = 8

#IP address of MQTT Broker
broker_address = "192.168.1.9"

client = mqtt.Client()
client.connect(broker_address, 1883, 60)

#read data from DHT22
humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

if humidity is not None and temperature is not None:
    #print 'Temp={0:0.1f} C  Humidity={1:0.1f}%'.format(temperature, humidity)
    sensor_data = "temp,temperature="+ str(temperature) + " humidity=" + str(humidity)
    print sensor_data
    client.publish("sensors", sensor_data )
else:
    print 'Failed to get reading. Try again!'

client.disconnect()
